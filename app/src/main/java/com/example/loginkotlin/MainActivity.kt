package com.example.loginkotlin

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlin.properties.Delegates

class MainActivity : AppCompatActivity() {

    private val TAG = "LoginActivity"

    //Variables Globales
    private var email by Delegates.notNull<String>()
    private var password by Delegates.notNull<String>()
    private lateinit var etEmail: EditText
    private lateinit var etPassword:EditText
    private lateinit var mProgressBar: ProgressDialog

    //Variable de Autentificacion Firebase
    private lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialise()
    }

    private fun initialise(){
        etEmail = findViewById(R.id.etEmail)
        etPassword = findViewById(R.id.etPassword)
        mProgressBar = ProgressDialog(this)
        mAuth = FirebaseAuth.getInstance()
    }

    //Ahora iniciamos sesion con firebase

    private fun loginUser(){
        //Obtenemos el usuario y contraseña
        email = etEmail.text.toString()
        password = etPassword.text.toString()
        //Verificamos que los campos no esten vacios
        if(!TextUtils.isEmpty(email)&&!TextUtils.isEmpty(password)){
            //Mostramos el ProgresDialog
            mProgressBar.setMessage("Registrar Usuario...")
            mProgressBar.show()

            //Iniciamos sesion con el metodo singIn y enviamos usuario
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this){
                //verificamos que la tarea se ejecuto
                task ->
                if(task.isSuccessful){
                   goHome()
                }else{
                    Toast.makeText(this, "Autentificacion Fallida", Toast.LENGTH_SHORT).show()
                }
            }
        }else{
            Toast.makeText(this, "Entrar todos los Detalles", Toast.LENGTH_SHORT).show()
        }
    }

    private fun goHome(){
        //Ocultamos el Progress
        mProgressBar.hide()
        //Regresamos al Home
        val intent = Intent(this, MainActivity:: class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
    }

    fun login(view: View){
        loginUser()
    }

    fun forgotPassword(view: View){
        startActivity(Intent(this, ForgotPasswordActivity::class.java))
    }

    fun register(view: View){
        startActivity(Intent(this, RegisterActivity::class.java))
    }
}
